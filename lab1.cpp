#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main()
{
	auto ch = "a";
	auto i= 1;
	auto name= "Pablo";
	
	cout << "Char is:" << ch << endl;
	cout << "Int is:" << i << endl;
	cout << "String is:" << name << endl;
	
	
	vector<int> vec;//vectors
	vec.push_back(5);
	vec.push_back(6);
	
	vec.pop_back();
	
	for ( unsigned int v=0; v<5; v++)//pushback
	{ 
		vec.push_back(v);
	}
	
	for (unsigned int a=0; a < vec.size(); a++)//printing
	{
		cout << "Vector: " << vec[a] <<endl;
	}
	
	for ( unsigned int l=0; l<1; l++)///pop_back
	{
		vec.pop_back();
	}
	
	for (auto vect: vec)
	{
		cout << vect <<endl;
	}
	
	
	//strings 
	string s="Success";
	cout << "string: " << s<<endl;
	
	if(s== "Success")
	{
		cout << "Success" << endl;
	}
	return 0;
}
